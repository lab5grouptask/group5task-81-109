
import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd


class Mainwindow(QMainWindow):
    def __init__(self):
        super(Mainwindow,self).__init__()
        loadUi("Part1.ui",self) 
        
       
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        
        self.Mini.clicked.connect(lambda: self.showMinimized())
        self.Cross.clicked.connect(lambda: self.close())
        self.SaveButton.clicked.connect(self.saveinfo)
        self.Party1Button.clicked.connect(loadUi("FirstParty.ui"))
      # def load_table(self):
    #     with open('BasicinfoDt.csv', "r",encoding="utf-8") as fileInput:
    #         roww = 0
    #         data = list(csv.reader(fileInput))
             
    #         self.StuInfoTable.setRowCount(len(data))
    #         for row in data:
    #             self.StuInfoTable.setItem(roww, 0 , QtWidgets.QTableWidgetItem((row[0])))
    #             self.StuInfoTable.setItem(roww, 1 , QtWidgets.QTableWidgetItem((row[1])))
    #             self.StuInfoTable.setItem(roww, 2 , QtWidgets.QTableWidgetItem((row[2])))
    #             self.StuInfoTable.setItem(roww, 3 , QtWidgets.QTableWidgetItem((row[3])))
    #             self.StuInfoTable.setItem(roww, 4 , QtWidgets.QTableWidgetItem((row[4])))
    #             roww =+ 1  
   
    def saveinfo(self):
        district = self.comboBox.currentText()
        tehsil = self.TehsilCombo.currentText()
        stamp_paper = self.StampCombo.currentText()
        deed_name = self.DeedCombo.currentText()
        a_name = self.lineEditName.text()
        a_cnic=self.lineEditCNIC.text()
        a_contact=self.lineEditContact.text()
        a_email=self.lineEditemail.text()
        
        Data=[district,tehsil,stamp_paper,deed_name,a_name,a_cnic,a_contact,a_email]
        try: 
            with open('BasicinfoDt.csv', 'a+',encoding="utf-8",newline="") as fileInput:
                writer = csv.writer(fileInput)
                writer.writerows([Data])
                                
        except Exception as e:
            print(e)
    


app = QApplication(sys.argv)
window = Mainwindow()
window.show()

sys.exit(app.exec_())